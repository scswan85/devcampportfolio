# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first

3.times do |topic|
  Topic.create!(
    title: "Topic #{topic}"
  )
end
10.times do |blog|
  Blog.create!(
    title: "My Blog Post #{blog}",
    body: "Dolore incididunt incididunt consectetur ipsum aute quis in proident exercitation enim culpa dolor pariatur. Cillum fugiat qui excepteur labore Lorem dolore est officia ea. Elit consectetur elit non commodo proident sit minim irure sint voluptate labore reprehenderit aliqua est laboris. Mollit irure occaecat in culpa voluptate mollit cillum excepteur qui excepteur incididunt deserunt deserunt voluptate mollit. Sit mollit dolore aliqua fugiat occaecat eiusmod Lorem quis tempor.",
    topic_id: Topic.last.id
  )
end

puts "10 blog posts created"

5.times do |skill|
  Skill.create!(
      title: "Rails #{skill}",
      percent_utilized: 15
  )
end

puts "5 skills created"

8.times do |portfolio_item|
  Portfolio.create!(
    title: "Portfolio Title #{portfolio_item}",
    subtitle: "Ruby on Rails",
    body: "Magna minim pariatur magna ut irure occaecat dolore non laboris. Proident sit irure minim nostrud nisi exercitation fugiat deserunt laboris esse. Eu qui ut nostrud nisi adipisicing veniam dolore. Amet officia do nulla cillum consequat elit occaecat. Veniam nisi non ipsum consequat sit laboris veniam culpa minim do eiusmod dolore pariatur id velit aute. Sit proident eiusmod pariatur nostrud veniam laboris sit consectetur sint laborum ut excepteur qui labore labore non ullamco. Cupidatat aliqua dolore enim irure qui in adipisicing elit magna officia aliqua.",
    main_image: "http://via.placeholder.com/600x400",
    thumb_image: "http://via.placeholder.com/350x200"
  )
end

1.times do |portfolio_item|
  Portfolio.create!(
    title: "Portfolio Title #{portfolio_item}",
    subtitle: "Angular",
    body: "Magna minim pariatur magna ut irure occaecat dolore non laboris. Proident sit irure minim nostrud nisi exercitation fugiat deserunt laboris esse. Eu qui ut nostrud nisi adipisicing veniam dolore. Amet officia do nulla cillum consequat elit occaecat. Veniam nisi non ipsum consequat sit laboris veniam culpa minim do eiusmod dolore pariatur id velit aute. Sit proident eiusmod pariatur nostrud veniam laboris sit consectetur sint laborum ut excepteur qui labore labore non ullamco. Cupidatat aliqua dolore enim irure qui in adipisicing elit magna officia aliqua.",
    main_image: "http://via.placeholder.com/600x400",
    thumb_image: "http://via.placeholder.com/350x200"
  )
end

puts "9 portfolio items created"

3.times do |technology|
  Portfolio.last.technologies.create!(
    name: "Technology #{technology}",
  )
end

puts "3 technologies created"
