class Portfolio < ApplicationRecord
  has_many :technologies, dependent: :destroy
  accepts_nested_attributes_for :technologies,
                                reject_if: lambda { |attrs| attrs['name'].blank? },
                                allow_destroy: true


  validates_presence_of :title, :body

  mount_uploader :thumb_image, PortfolioUploader
  mount_uploader :main_image, PortfolioUploader


  # Does the same thing as scope but not a best practice.
  def self.angular
    where(subtitle: "Angular")
  end

  # Considered a best practice
  scope :ruby_on_rails_portfolio_items, -> { where(subtitle: "Ruby on Rails") }

  def self.by_position
    self.order("position ASC")
  end
end
