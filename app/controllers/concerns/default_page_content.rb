module DefaultPageContent
  extend ActiveSupport::Concern

  included do
    before_action :set_page_defaults
  end

  def set_page_defaults
    @page_title = "Portfolio | Stephen Swann"
    @seo_keywords = "Stephen Swann Portfolio"
  end
end
