class PagesController < ApplicationController
  def home
    @posts = Blog.all
    @skills = Skill.all
    @portfolios = Portfolio.all
  end

  def about
    @page_title = "About"
  end

  def contact
    @page_title = "Contact"
  end

  def tech_news
    @tweets = SocialTool.twitter_search
  end
end

